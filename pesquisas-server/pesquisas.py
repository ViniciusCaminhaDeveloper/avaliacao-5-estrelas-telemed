from flask import Flask, render_template, request, url_for, jsonify
from flask_cors import CORS, cross_origin
import json
from datetime import datetime
import pymongo

app = Flask(__name__)
CORS(app, support_credentials=True)

@app.route("/pesquisas", methods=["POST"])
@cross_origin(supports_credentials=True)
def hello():

    # conexao db
    mongo_connect = "mongodb://vinicius:vinicius@10.1.16.134:37017/uptime"
    myclient = pymongo.MongoClient(mongo_connect)
    mydb = myclient["uptime"]
    mycol = mydb["pesquisas"]
    mycol2 = mydb["atendimentos"]




    input_json = request.get_json(force=True) 
    # print(input_json[0])

    objeto = '{ "codigo_atendimento": "", "DATA": "", "DATA_CRIACAO": "", "avaliacao": "", "avaliacao_display": "", "canal": "", "cdNaturezaJuridica": "", "cidade": "", "codigo": "", "codigo_usuario": "", "comentario": "", "contexto": "HOSP", "contexto_display": "Hospitalar", "data_atendimento": "", "data_criacao": "", "ds_motivo": "EMERGENCIA", "especialidade": { "codigo": "1", "nome": "CLINICA MEDICA" }, "exame": "false", "flInternacao": "false", "flOdonto": "false", "local": { "codigo": "", "nome": "" }, "motivo": "EMERGENCIA", "natureza_juridica": "Rede Propria", "pessoa": { "codigo": "", "cpf": "", "nome": "" }, "profissional": { "codigo": "", "nome": "" }, "uf": "", "url": "", "votos": { "acolhimento": false, "ambiente": false, "procedimentos": false, "profissional_de_saude": false, "tempo_de_espera": false } }'
    objetoParse = json.loads(objeto)

    objetoParse["codigo_atendimento"] = input_json[0]['codigo_atendimento']
    objetoParse["DATA"] =  datetime.strptime(input_json[1]['results'][0]['data_atendimento'], '%Y-%m-%d')
    objetoParse["DATA_CRIACAO"] = datetime.strptime(input_json[1]['results'][0]['data_atendimento'], '%Y-%m-%d')
    objetoParse["avaliacao"] = int(input_json[0]['avaliacao'])

    # Logica da avaliação display.
    if input_json[0]['avaliacao'] == '1' :
        objetoParse["avaliacao_display"] = 'Péssimo'
    if input_json[0]['avaliacao'] == '2' :
        objetoParse["avaliacao_display"] = 'Ruim'
    if input_json[0]['avaliacao'] == '3' :
        objetoParse["avaliacao_display"] = 'Pode Melhorar'
    if input_json[0]['avaliacao'] == '4' :
        objetoParse["avaliacao_display"] = 'Gostei'
    if input_json[0]['avaliacao'] == '5' :
        objetoParse["avaliacao_display"] = 'Adorei'

    objetoParse["canal"] = "app"
    objetoParse["cidade"] = input_json[1]['results'][0]['cidade']
    objetoParse["cdNaturezaJuridica"] = 21
    # objetoParse["codigo"] = input_json[0]['codigo_atendimento']
    objetoParse["codigo_usuario"] = input_json[1]['results'][0]['cdUsuario']
    objetoParse["comentario"] = input_json[0]['comentario']
    objetoParse["contexto"] = "HOSP"
    objetoParse["contexto_display"] = "TELECONSULTA"
    objetoParse["data_atendimento"] = input_json[1]['results'][0]['data_atendimento']
    objetoParse["data_criacao"] = input_json[1]['results'][0]['data_atendimento']
    # Local
    objetoParse["local"]['codigo'] = input_json[1]['results'][0]['local']['codigo']
    objetoParse["local"]['nome'] = input_json[1]['results'][0]['local']['nome']
    # Pessoa
    objetoParse["pessoa"]['codigo'] = input_json[1]['results'][0]['pessoa']['codigo']
    objetoParse["pessoa"]['cpf'] = input_json[1]['results'][0]['pessoa']['cpf']
    objetoParse["pessoa"]['nome'] = input_json[1]['results'][0]['pessoa']['nome']
    # Profissional
    objetoParse["profissional"]['codigo'] = input_json[1]['results'][0]['profissional']['codigo']
    objetoParse["profissional"]['nome'] = (input_json[1]['results'][0]['profissional']['nome']).upper()
    # Extra
    objetoParse["uf"] = input_json[1]['results'][0]['uf']

    # Votos
    
    for elogio in input_json[0]['codigos_elogios']:
        if(elogio == '1'):
            objetoParse["votos"]['profissional_de_saude'] = True
        elif(elogio == '2'):
            objetoParse["votos"]['tempo_de_espera'] = True
        elif(elogio == '3'):
            objetoParse["votos"]['videochamada'] = True
        elif(elogio == '4'):
            objetoParse["votos"]['receita_digital'] = True
        elif(elogio == '5'):
            objetoParse["votos"]['usabilidade'] = True

    # Objetos para atendimetno
    objetoParse["cdPessoa"] = input_json[1]['results'][0]['cdUsuario']
    objetoParse["cdStatus"] = 1
    objetoParse["flUrgencia"] = "EMERGENCIA",
    objetoParse["flUrgencia"] = True

    print(objetoParse)


    mycol.insert_one(objetoParse)
    mycol2.insert_one(objetoParse)
    return '{"return" : "Avaliação realizada com sucesso!"}'


if __name__ == "__main__":
    app.run(debug=True)